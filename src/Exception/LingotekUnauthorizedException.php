<?php

namespace Drupal\lingotek\Exception;

/**
 * The Lingotek Api Exception class.
 */
class LingotekUnauthorizedException extends LingotekApiException {

}
